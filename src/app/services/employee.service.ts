import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map, retry, catchError } from 'rxjs/operators';
import { of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  headers = new HttpHeaders();
  constructor(private http: HttpClient) {
    this.headers.append('Content-Type', 'application/json');
  }

  // create an employee
  createEmployee(employee: any) {
    // api url called : 'http://dummy.restapiexample.com/create'
    return this.http.post(environment.baseURL + 'create', employee, { headers: this.headers }).pipe(retry(3), map(res => {
      if (!res) {
        throw new Error('Response Expected');
      }
      return res;
    }),
    catchError(err => of([]))
    );
  }
}
