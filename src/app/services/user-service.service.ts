import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UserServiceService {

  Tasks = [];

  constructor() { }

  getTasks(){
    return this.Tasks;
  }

  addTask(task) {
    this.Tasks.push(task);
    return this.Tasks;
  }


}
