import { Injectable } from '@angular/core';
import { Task } from '../interfaces/task';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { map, retry, catchError } from 'rxjs/operators';
import { of } from 'rxjs';


interface taskResponse {
  message?: string,
  tasks?: Task[]
}

@Injectable({
  providedIn: 'root'
})



export class TodoService {

  Tasks : Task[] = [
    { title: 'title1', status: true},
    { title: 'title2', status: true},
    { title: 'title3', status: true},
    { title: 'title4', status: true}
  ];

  headers = new HttpHeaders();

  task = {} as Task;
  constructor(
    public http : HttpClient
  ) {
      this.headers.append('Content-Type','application/json')
  }


  getTasks(){
    console.log(this.Tasks);
    return this.Tasks;
  }

  getTasksAPI(){
    return this.http.get('http://192.168.1.131:3000/tasks').pipe(map((res : taskResponse) => {
      if(!res){
        console.log('no response');
      };
      let response = res;
      return response.tasks;
    }));
  }

  addTasksAPI(task: Task){
    return this.http.post('http://192.168.1.131:3000/task', task).pipe(map((res : taskResponse) => {
      if(!res){
        console.log('no response');
      };
      let response = res;
      return response.tasks;
    }));
  }


  getTasksLocalStorage(){
    let tasks = JSON.parse(localStorage.getItem('Tasks'));
    if(tasks) {
      return tasks;
    } else {
      return [];
    }
  }

  addTaskLocalStorage(task: Task){
    task.status = true;
    this.Tasks.push(task);
    localStorage.setItem('Tasks',JSON.stringify(this.Tasks));
    return this.Tasks;
  }

  addTask(task: Task){
    task.status = true;
    this.Tasks.push(task);
    return this.Tasks;
  }

  deleteTask(task: Task){
    this.Tasks.splice(this.Tasks.findIndex((e) => e.title === task.title ), 1);
    return this.Tasks;
  }
}
