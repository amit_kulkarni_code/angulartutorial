import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from '../components/login/login.component';
import { DashboardComponent } from '../components/dashboard/dashboard.component';
import { DefaultComponent } from '../components/default/default.component';
import { RecipeDetailComponent } from '../components/recipe-detail/recipe-detail.component';
import { TodoComponent } from '../components/todo/todo.component';

let routes : Routes = [
  { path:"" , redirectTo:"login", pathMatch:"full"},
  { path:"login" , component:LoginComponent},
  { path:"detail/:id" , component:RecipeDetailComponent},
  { path:"dashboard" , component:DashboardComponent},
  { path:"todo" , component:TodoComponent},
  { path:"**" , component:DefaultComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class appRoutingModule { }
