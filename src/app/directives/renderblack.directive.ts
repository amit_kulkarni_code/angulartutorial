import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[appRenderblack]'
})
export class RenderblackDirective {

  element : ElementRef;
  change = false;
  constructor(public el: ElementRef) {
     console.log(el);
     this.element = el;
   }

   ngOnInit(): void {
     this.element.nativeElement.style.color = 'red';

     setInterval(() => {
       this.change = !this.change;
       if(this.change) {
        this.element.nativeElement.style.background = 'yellow';
      } else {
         this.element.nativeElement.style.background = 'black';
      }
     }, 2000);
     this.element.nativeElement.innerText += ' - This is a custom directive added to the text';
   }

}
