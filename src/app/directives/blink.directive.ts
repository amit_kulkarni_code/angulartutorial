import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[appBlink]'
})
export class BlinkDirective {
 
  element : ElementRef;
  blink: boolean = false;
  
  constructor(el : ElementRef) {
     console.log(el);
     el.nativeElement.style.color = 'red';
     el.nativeElement.style.border = '1px solid black';
     el.nativeElement.style.background = 'yellow';
      this.element = el;
    }

    ngOnInit(){
      this.element.nativeElement.innerText += " - this is a custom directive"
      setInterval(()=>{
        this.blink = !this.blink;
        if(this.blink){
          this.element.nativeElement.style.background = "black";
          this.element.nativeElement.style.color = "white";
        } else {
          this.element.nativeElement.style.color = 'red';
     this.element.nativeElement.style.border = '1px solid black';
     this.element.nativeElement.style.background = 'yellow';  
        }
      }, 1000);
    }
}
