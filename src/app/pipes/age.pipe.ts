import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'age'
})
export class AgePipe implements PipeTransform {

  transform(value: any, ...args: any[]): any {
    const currentYear = new Date().getFullYear();
    const birthYear = new Date(value).getFullYear();
    console.log(birthYear);
    const age = currentYear - birthYear;
    return age;
  }

}
