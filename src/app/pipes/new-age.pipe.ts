import { Pipe, PipeTransform } from '@angular/core';
import { Binary } from '@angular/compiler';

@Pipe({
  name: 'newAge'
})
export class NewAgePipe implements PipeTransform {

  transform(value: any, ...args: any[]): any {
   console.log(value);
   let currentYear = new Date().getFullYear();
   console.log(currentYear);
   let birthYear = new Date(value).getFullYear();
   console.log(birthYear);
   let age = currentYear - birthYear;
   let text = age + " years old";
   if(age < 0) {
    text =  "please enter correct year";
   }
   return text;
  }

}
