import { httpIntercepterProvider } from './http-intercepter/index';
import { AppRoutingModule , routingComponents } from './app.routing.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { ParentComponent } from './components/parent/parent.component';
import { ChildComponent } from './components/child/child.component';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HttpClientModule } from '@angular/common/http';
import { TodoComponent } from './components/todo/todo.component';
import { TaskListComponent } from './components/task-list/task-list.component';
import { TaskAddComponent } from './components/task-add/task-add.component';
import { LoginComponent } from './components/login/login.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { appRoutingModule } from './routing/app.routing.module';
import { DefaultComponent } from './components/default/default.component';
import { RecipeDetailComponent } from './components/recipe-detail/recipe-detail.component';
import { AgePipe } from './pipes/age.pipe';
import { RenderblackDirective } from './directives/renderblack.directive';
import { NewAgePipe } from './pipes/new-age.pipe';
import { BlinkDirective } from './directives/blink.directive';

@NgModule({
  declarations: [
    AppComponent,
    ParentComponent,
    ChildComponent,
    TodoComponent,
    TaskListComponent,
    TaskAddComponent,
    LoginComponent,
    DashboardComponent,
    DefaultComponent,
    RecipeDetailComponent,
    AgePipe,
    RenderblackDirective,
    NewAgePipe,
    BlinkDirective
  ],
  imports: [
    NgbModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    appRoutingModule
  ],
  providers: [httpIntercepterProvider],
  bootstrap: [AppComponent]
})
export class AppModule { }
