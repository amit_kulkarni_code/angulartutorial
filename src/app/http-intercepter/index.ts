import { HeaderIntercepter } from './header-intercepter';
import { HTTP_INTERCEPTORS } from '@angular/common/http';

export const httpIntercepterProvider = [
  {
    provide: HTTP_INTERCEPTORS ,
    useClass: HeaderIntercepter,
    multi: true
  }
]
