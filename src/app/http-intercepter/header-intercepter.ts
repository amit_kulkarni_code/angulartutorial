import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler } from '@angular/common/http';

@Injectable()
export class HeaderIntercepter implements HttpInterceptor {

      intercept( req: HttpRequest<any>, next: HttpHandler){
        console.log('intercepter');
        console.log(req);
        const authReq = req.clone({ headers: req.headers.set('Authorization', 'Bearer ' + 'asdkjashdjkasdhkajsdajksd' ) });
        return next.handle(authReq);
      }
}
