import { element } from 'protractor';
import { Component, OnInit } from '@angular/core';
import { Task } from '../../interfaces/task';
import { TodoService } from 'src/app/services/todo.service';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})


export class TodoComponent implements OnInit {

  Tasks: Task [] = [];
  task: Task = { title : 'sdsd'};

  constructor(
    public todoService : TodoService
  ){  
      console.log(todoService.getTasks());
      todoService.getTasksAPI().subscribe((response) => {
        console.log('api called',response);
        this.Tasks = response;
      });
      
      console.log(todoService.getTasksLocalStorage());
      //this.Tasks = todoService.getTasksLocalStorage();
      //this.Tasks = todoService.getTasks();
  }

  ngOnInit() {
  }

  onSubmit() {
    this.Tasks = this.todoService.addTask(this.task);
    this.todoService.addTasksAPI(this.task).subscribe((response) => {
      console.log(response);
      this.Tasks = response;
    })
   /// this.Tasks = this.todoService.addTaskLocalStorage(this.task);
  }

  onDone(task: Task) {
    const index = this.Tasks.findIndex((e) => e.title === task.title);
    this.Tasks[index].status = !this.Tasks[index].status;
    console.log(this.Tasks);
  }

  onRemove(task: Task) {
    this.Tasks = this.todoService.deleteTask(task);
  }

  getTask(event) {
    console.log(event);
    let task ;
    task = event;
    this.Tasks.push(task);
    console.log(this.Tasks);
  }

}
