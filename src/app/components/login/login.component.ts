import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validator, Validators} from '@angular/forms';
import { password } from './customValidator';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  form : FormGroup;

  constructor(
     fb : FormBuilder
  ) {
      this.form = fb.group({
        email: ['' , [Validators.email, Validators.required]],
        password: ['', Validators.required, password]
      });
   }

  ngOnInit() {
  }

  onSubmit(){
    console.log(this.form.value);

    console.log(this.form.get('email'));
    this.form.get('email').dirty;
  }
}
