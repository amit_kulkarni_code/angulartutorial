import { AbstractControl } from "@angular/forms"

export function password(control: AbstractControl) {
  if (control && (control.value !== null || control.value !== undefined)) {
      const regx = new RegExp('^[a-zA-Z]\w{3,14}$');
      if (!regx.test(control.value)) {
        return true;
      }
  }

  return true;
}
