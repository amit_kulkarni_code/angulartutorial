import { EmployeeService } from './../../services/employee.service';
import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup , FormControl, Validators, FormBuilder} from '@angular/forms';

@Component({
  selector: 'app-child',  
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.css']
})
export class ChildComponent implements OnInit {

  @Input() public greeting: string;
  @Output() public sendOutput = new EventEmitter();

  eventName;
  showSpinner = false;
  user: Object = {};
  employee: Object = {
    name: '',
    salary: '',
    age: ''
  };

  form: FormGroup;
  employeeForm: FormGroup;
  tasks = [];
  task = new FormControl('', Validators.required);
  constructor(
    fb: FormBuilder,
    public empService: EmployeeService
    ) {
      this.form = fb.group({
        'task': new FormControl('', Validators.required)
      });

      this.employeeForm = fb.group({
        name: ['', [Validators.required]],
        salary: ['', Validators.required],
        age: ['', Validators.required]
      });
  }

  ngOnInit() {
  }

  submitForm() {
    console.log(this.eventName);
    console.log(this.user);
  }

  onSubmit() {
    console.log(this.form.value.task);
    this.addTask(this.form.value.task);
    this.reset();
    console.log(this.tasks);
  }

  addTask(task) {
    this.tasks.push(task);
  }

  reset() {
    this.form.reset();
  }
  sendEvent() {
    console.log('event sendt');
    this.sendOutput.emit('button clicked');
  }

   // create use function
  createEmployee() {
    this.showSpinner = true;
    console.log(this.employeeForm.value);
    // create employee post call
    this.empService.createEmployee(this.employeeForm.value).subscribe((response) => {
      console.log(response);
      if (response) {
        this.showSpinner = false;
      }
    });
  }

}
