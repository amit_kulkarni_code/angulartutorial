import { Component, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-parent',
  templateUrl: './parent.component.html',
  styleUrls: ['./parent.component.css']
})
export class ParentComponent implements OnInit {

  greetings: string;
  greeting: string = "good morning";
  constructor() { }

  ngOnInit() {
    setInterval(() => {

      this.greeting = new Date().toISOString();

  }, 1000);
  }

  showGreeting(key) {
    if (key === 'afternoon') {
      this.greetings = 'good morning';
    } else if (key === 'afternoon') {
      this.greetings = 'good afternoon';
    } else {
      this.greetings = 'good night';
    }
    return this.greetings;
  }

  public getOutput(output) {
    console.log('output sent ' , output);
  }

}
