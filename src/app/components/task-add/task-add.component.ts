import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-task-add',
  templateUrl: './task-add.component.html',
  styleUrls: ['./task-add.component.css']
})
export class TaskAddComponent implements OnInit {

  @Output() public sendTask = new EventEmitter();
  task = {
    title: ''
  };

  constructor() { }

  onSubmit() {
    console.log(this.task);
    this.sendTask.emit(this.task);
  }

  ngOnInit() {
  }

}
